import {
  AmbitLogo,
  BarepapersLogo,
  BimLogo,
  CDGOLogo,
  ClevertechLogo,
  ConsultlyLogo,
  EsieeLogo,
  EvercastLogo,
  Howdy,
  JarockiMeLogo,
  JojoMobileLogo,
  Minimal,
  MFSLogo,
  MobileVikingsLogo,
  MonitoLogo,
  NSNLogo,
  ParabolLogo,
  TastyCloudLogo,
  UmlvLogo,
  YearProgressLogo,
} from "@/images/logos";
import { GitHubIcon, GitlabIcon, LinkedInIcon, XIcon } from "@/components/icons";

export const RESUME_DATA = {
  name: "Nicolas Dewaele",
  initials: "ND",
  location: "Lagny sur Marne, France",
  locationLink: "https://www.openstreetmap.org/search?query=lagny%20sur%20marne#map=12/48.8716/2.7045",
  about:
    "Responsable cloud. Pilotage de projets AWS et GCP",
  summary:
    "Je travaille depuis 20 ans dans les infrastructures IT, toujours aussi passionné notamment par le mouvement Devops, le cloud, la sécurité. \
    J'ai pu réaliser et piloter de nombreux projets d'architectures IT, aujourd'hui chez AWS et GCP.",
  avatarUrl: "https://gitlab.com/uploads/-/system/user/avatar/3565244/avatar.png?width=400",
  personalWebsiteUrl: "https://dans-les-nuages.fr",
  contact: {
    social: [
      {
        name: "LinkedIn",
        url: "https://www.linkedin.com/in/nicolas-dewaele/",
        icon: LinkedInIcon,
      },
    ],
  },
  education: [
    {
      school: "CNAM",
      degree: "Ingénieur systèmes, réseaux  et multimédia",
      start: "2008",
      end: "2016",
    },
    {
      school: "Université de Cergy-Pontoise",
      degree: "Licence professionnelle informatique et communication",
      start: "2002",
      end: "2003",
    },
  ],
  work: [
    {
      company: "Mobilize Financial Services",
      link: "https://www.mobilize-fs.fr",
      badges: ["Hybride"],
      title: "Responsable cloud",
      logo: MFSLogo,
      start: "2023",
      end: "Today",
      description:
        "Conduite de projets cloud AWS et GCP. Modernisation d'infrastractures avec de l'infrastructure as code et une chaine CI/CD. Technologies: AWS, GCP, Devops, CI/CD",
    },
    {
      company: "Mobilize Financial Services",
      link: "https://www.mobilize-fs.fr",
      badges: ["Hybride"],
      title: "Responsable de la sécurité informatique",
      logo: MFSLogo,
      start: "2020",
      end: "2023",
      description:
        "Gouvernance de la sécurité informatique. Accompagnement pour la mise en place d'architectures sécurisées. Organisation d'audits et de pentests. Accompagnement sécurité dans les projets. Technologies: AWS, GCP, Devops, CI/CD, F-Secure, Qualys, ",
    },
    {
      company: "Esiee Paris",
      link: "https://www.esiee.fr",
      badges: ["Hybride"],
      title: "Responsable de la sécurité des systèmes d'information",
      logo: EsieeLogo,
      start: "2016",
      end: "2020",
      description:
        "Application de la PSSI groupe au système d'information local. Gouvernance de la sécurité informatique. Accompagnement pour la mise en place d'architectures sécurisées. Technologies: Linux, Windows, WAF Modsecurity, Proxmox VE, Docker, Devsecops",
    },
    {
      company: "Esiee Paris",
      link: "https://www.esiee.fr",
      badges: ["Sur site"],
      title: "Ingénieur systèmes et réseaux",
      logo: EsieeLogo,
      start: "2016",
      end: "2014",
      description:
        "Mise en place et maintenance d'architectures systèmes et réseaux. Modernisation des systèmes (Virtualisation, Contenerisation, Microservices). Technologies: Linux, Windows, Proxmox VE, Docker, Devops, Cisco, Meraki, Palo Alto, Modsecurity",
    },
    {
      company: "Université Paris Est MLV",
      link: "https://www.univ-gustave-eiffel.fr/",
      badges: ["Sur site"],
      title: "Ingénieur réseau",
      logo: UmlvLogo,
      start: "2014",
      end: "2014",
      description:
        "Administration, maintenance et supervision du réseau métropolitain (MAN) du campus universitaire. Technologies: Brocade, Cisco, Linux, Nagios, Cacti",
    },
    {
      company: "Greta Tertiaire 77",
      link: "https://www.greta-iledefrance.fr/",
      badges: ["Sur site"],
      title: "Formateur systèmes, réseaux, sécurité",
      start: "2003",
      end: "2014",
      description: "Conception et animation de formations informatiques. Technologies: Linux, Windows, Cisco, Certifications CCNA, Sécurité, PHP.",
    },
  ],
  skills: [
    "Conduite de projets",
    "Architecture",
    "GCP",
    "Compute Engine",
    "GKE",
    "Cloud Run",
    "Cloud Functions",
    "Cloud SQL",
    "AWS",
    "EKS",
    "EC2",
    "Beanstalk",
    "Infra as code",
    "Terraform",
    "Devops",
    "Docker",
    "Kubernetes",
    "CI/CD",
    "Gitlab",
    "Réseau",
    "Cisco",
    "Linux",
    "Windows",
    "Ruby on Rails",
  ],
  projects: [
    {
      title: "Secboard",
      techStack: [
        "Side Project",
        "Docker",
        "Ruby",
        "Rails",
      ],
      description: "A dashboard to follow security alerts and remediations",
      logo: ConsultlyLogo,
      link: {
        label: "secboard on Gitlab",
        href: "https://gitlab.com/ndew/secboard",
      },
    },
    {
      title: "Clapi",
      techStack: ["Side Project", "Docker", "ClamAV", "Ruby"],
      description:
        "ClamAV API : Need to check a file before uploading it ? Ask to ClamAV \
        an open source Anti Virus by Cisco Talos with lots of extra signatures sources.",
      logo: MonitoLogo,
      link: {
        label: "Clapi on Gitlab",
        href: "https://gitlab.com/ndew/clapi",
      },
    },
    {
      title: "Grépon",
      techStack: [
        "Side Project",
        "Docker",
        "Ruby",
        "Rails",
      ],
      description: "A web security scanner",
      logo: ConsultlyLogo,
      link: {
        label: "Grépon on Gitlab",
        href: "https://gitlab.com/ndew/grepon",
      },
    },
  ],
} as const;
