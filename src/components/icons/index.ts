import { GitHubIcon } from "./GitHubIcon";
import { GitlabIcon } from "./GitlabIcon";
import { LinkedInIcon } from "./LinkedInIcon";
import { XIcon } from "./XIcon";

export { GitHubIcon, GitlabIcon, LinkedInIcon, XIcon };
